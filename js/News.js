$(function () {
  $.ajax({
    url: 'https://hn.algolia.com/api/v1/search?query=coronavirus-vaccine',
    dataType: 'json',
    success: function (json) {
      $('#NewsLoadError').hide();

      const News = json.hits.slice(0, 6);
      News.map((news, index) => {
        console.log(news.url);
        let containar = `<div class="col-sm-4"><div class="card text-center"><div class="card-body text-center">`;
        containar += `<h5 class="card-title">${news.title}</h5>`;
        containar += `<a   href="${news.url}" class="btn btn-info">more</a>`;
        containar += `</div></div<div>`;
        $('#News').append(containar);
      });
      $('#NewsLoading').hide();
    },
    error: function (data) {
      $('#NewsLoadError').text('There is problem to loading News');
      $('#NewsLoading').hide();
    }
  });
});
